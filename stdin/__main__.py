from parcer_service import Parser


def main():
    parser = Parser()
    while True:
        search_word = input()
        parser.find_thread(search_word)


if __name__ == '__main__':
    main()
