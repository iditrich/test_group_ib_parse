from fastapi import APIRouter, status, Depends, Path, Request
from parcer_service.service import Parser
from parcer_service.models import ParserRequestV1, ParserResponseV1, ParserErrorResponseV1
from fastapi.responses import JSONResponse

router = APIRouter(
    tags=['Parser']
)
parser = Parser()


@router.put(
    path='/v1/search',
    response_model=ParserResponseV1,
    responses={500: {"model": ParserErrorResponseV1}},
    summary='Поиск по слову',
    description='Парсит первые 10 товаров по запросу введенного слова.'
)
async def search(
        distribution_data: ParserRequestV1,
):
    result, data = await parser.find_async(distribution_data.searchWord, distribution_data.proxy)
    if result:
        return JSONResponse(status_code=200, content=data)
    else:
        return JSONResponse(status_code=500, content=data)
