from fastapi import FastAPI
from api.parser import router


def get_application() -> FastAPI:
    application = FastAPI(
        title='Aliexpress search parser',
        description='Сервис для парсинга первых десяти товаров Aliexpress поиска по слову.',
        version='1.0.0'
    )
    application.include_router(router)

    return application


app = get_application()