import requests
from threading import Thread, RLock
from bs4 import BeautifulSoup
import re
import aiohttp

from settings import headers, requests_timeout


class Parser:
    workers = []
    proxy = {}

    def parse_description(self, obj):
        for i in obj.find_all("meta"):
            if 'name' in i.attrs and (i.attrs['name'] == 'description'):
                return i.attrs['content']

    def parse_title(self, obj):
        return obj.find("title").contents[0]

    def parse_price(self, obj):
        price_string = obj.find_all("div", class_='snow-price_SnowPrice__mainM__2y0jkd')[0].contents[0]
        price_string = price_string.replace(' руб.', '')

        # delete unicode symbols
        price_string = price_string.encode("ascii", "ignore").decode()

        price_string = price_string.replace(',', '.')
        return float(price_string)

    def create_request(self, url: str, proxies: str = None):
        return requests.get(url, headers=headers, timeout=requests_timeout, proxies=proxies).text

    def parse_thread(self, search_word: str):
        with RLock():

            if not search_word:
                print({'error': 'empty word'})
                return {'error': 'empty word'}

            url = f'https://aliexpress.ru/wholesale?SearchText={search_word}'
            result_data = {'search': search_word, 'result': []}

            try:
                search_response = self.create_request(url)

                search_soup = BeautifulSoup(search_response, features="html.parser")
                count = 0
                for i in search_soup.find_all("div", {"data-product-id": re.compile(r".*")}):
                    if ('data-product-id' in i.attrs) and (count < 10):
                        product_url = f"https://aliexpress.ru/item/{i.attrs['data-product-id']}.html"
                        product_response = self.create_request(product_url)

                        product_soup = BeautifulSoup(product_response, features="html.parser")

                        data = {}
                        data['url'] = product_url
                        data['title'] = self.parse_title(product_soup)
                        data['description'] = self.parse_description(product_soup)
                        data['price'] = self.parse_price(i)

                        result_data['result'].append(data)

                        count += 1

                print(100 * '-')
                self.beautiful_print(result_data)
                return result_data
            except requests.exceptions.ConnectionError:
                print({'error': 'connection error'})
                return {'error': 'connection error'}
            except requests.exceptions.Timeout:
                print({'error': 'connection timeout'})
                return {'error': 'connection timeout'}
            except BaseException as err:
                print({'error': str(type(err).__name__)})
                return {'error': str(type(err).__name__)}

    async def parse_async(self, search_word: str = None, proxy: str = None):
        if not search_word:
            return False, {'error': 'empty word'}

        url = f'https://aliexpress.ru/wholesale?SearchText={search_word}'
        result_data = {'search': search_word, 'result': []}

        try:
            search_response = await self.create_request_async(url, proxy)

            search_soup = BeautifulSoup(search_response, features="html.parser")
            count = 0
            for i in search_soup.find_all("div", {"data-product-id": re.compile(r".*")}):
                if ('data-product-id' in i.attrs) and (count < 10):
                    product_url = f"https://aliexpress.ru/item/{i.attrs['data-product-id']}.html"
                    product_response = await self.create_request_async(product_url, proxy)

                    product_soup = BeautifulSoup(product_response, features="html.parser")

                    data = {}
                    data['url'] = product_url
                    data['title'] = self.parse_title(product_soup)
                    data['description'] = self.parse_description(product_soup)
                    data['price'] = self.parse_price(i)

                    result_data['result'].append(data)

                    count += 1
            print(40 * '-')
            self.beautiful_print(result_data)
            return True, result_data
        except requests.exceptions.ConnectionError:
            print({'error': 'connection error'})
            return False, {'error': 'connection error'}
        except requests.exceptions.Timeout:
            print({'error': 'connection timeout'})
            return False, {'error': 'connection timeout'}
        except BaseException as err:
            print({'error': str(type(err).__name__)})
            return False, {'error': str(type(err).__name__)}

    async def create_request_async(self, url: str, proxy: str = None):
        async with aiohttp.ClientSession(headers=headers, connector=aiohttp.TCPConnector(verify_ssl=False)) as session:
            async with session.get(url, proxy=proxy) as response:
                return await response.text()

    def beautiful_print(self, obj):
        if type(obj) == dict:
            for k, v in obj.items():
                print('"' + k + '" ', end=':')
                self.beautiful_print(v)
        elif type(obj) == list:
            for v in obj:
                print()
                self.beautiful_print(v)
        else:
            print(obj)

    async def find_async(self, search_word: str, proxy: str = None) -> object:
        return await self.parse_async(search_word, proxy)

    def find_thread(self, search_word: str):
        if len(self.workers) >= 3:
            print('awaiting worker')
            self.workers[0].join()

        worker = Thread(target=self.parse_thread, kwargs={'search_word': search_word}, daemon=True)
        worker.start()
        self.workers.append(worker)
