from pydantic import BaseModel, Field, validator
from typing import Optional, List


class ParserRequestV1(BaseModel):
    searchWord: str
    proxy: Optional[str]


class ProductData(BaseModel):
    title: str
    description: str
    url: str
    price: int


class ParserResponseV1(BaseModel):
    search: str
    result: Optional[List[ProductData]]


class ParserErrorResponseV1(BaseModel):
    error: str
