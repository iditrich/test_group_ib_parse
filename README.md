# Test task group ib

## Service for parsing first 10 products found by search word on aliexpress

### Start app reading stdin:

```bash
pip3 install -r requirements.txt
python3 stdin
```

### Start app as web server:

```bash
pip3 install -r requirements.txt
uvicorn api:app --host 0.0.0.0 --port 5000
```

### App reading stdin:

+ read search word from stdin (example: test1<enter>)
+ cannot read proxy

### App as web server:

+ request model: "searchWord":str, "proxy":str
+ docs page: [0.0.0.0:5000/docs](http://0.0.0.0:5000/docs)

#### Ps: could not understand how to read prosy from stdin.

#### Ps1: could not understand how to return result from threads, used async.

#### Ps2: I think whole service should be written using asyncio.
